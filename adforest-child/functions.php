<?php

// Register our Google maps API key

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
function my_acf_google_map_api( $api ){
    $api['key'] = 'AIzaSyAOwNN2-YZgoOCj0iYxWdgHtS5MtQyOk1c';
    return $api;
}

add_action('wp_enqueue_scripts', 'child_theme_enqueue_styles');
function child_theme_enqueue_styles()
{
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style(
        'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array('parent-style'),
        '0.0.2'
    );
}